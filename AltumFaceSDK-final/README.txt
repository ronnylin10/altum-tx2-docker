To setup the image for altumboilerplate:

1) navigate to directory containing the docker container tar file. Currently the file name is tx2-opencv-setup-noswap-sdkbuilt.tar

2) cat tx2-opencv-setup-noswap-sdkbuilt.tar | docker import - altumface:new
	- This imports the tar file as a docker image with the name altumface and the tag new.

3) Once the image has been imported (check with docker images command), navigate to the directory containing the wrapper script for running this
   image. Currently the name of this script file is tx2-docker-altum.

4) sudo ./tx2-docker-altum run altumface

Notes:
- make sure camera is plugged in before you run the image.
- the image is very large and docker save does not work. Any content created inside the container will be lost if you do not export the    container to an external storage or commit to image. Also, might need to remove this image to run other images.

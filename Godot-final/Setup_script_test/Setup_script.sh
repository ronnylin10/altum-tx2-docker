#!/bin/bash

#Check check if Docker is working

GODOT_IMAGE_NAME="godot:latest"
GODOT_CONTAINER_NAME="run_godot"
GODOT_DOCKER_IMG="/home/nvidia/Desktop/Final/godot.tar"
WRAPPER_SCRIPT="/home/nvidia/Desktop/Final/tx2-docker"
WRAPPER_SCRIPT_ARG="run $GODOT_IMAGE_NAME"

sudo docker load --input $GODOT_DOCKER_IMG
sleep 3
sudo $WRAPPER_SCRIPT $WRAPPER_SCRIPT_ARG
sleep 3
echo "Saving your changes now, it will take a moment."
sleep 3
sudo docker commit $GODOT_CONTAINER_NAME $GODOT_IMAGE_NAME
sleep 3
echo "Almost there, just a moment..."
sleep 3
sudo docker save -o $GODOT_DOCKER_IMG $GODOT_IMAGE_NAME
sleep 3
echo "Removing artifacts now"
sleep 3
sudo docker rm $GODOT_CONTAINER_NAME
sleep 3
echo "Done!"


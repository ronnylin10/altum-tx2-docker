#!/bin/bash

# Function for periodically checking if Godot has stopped
Check_Godot()
{
  while sleep 3; do
    if pgrep -x "godot.x11.tools" > /dev/null
    then
        sleep 1
    else
        echo "exited"
        exit
    fi 
  done
}

# Run Godot 
/Godot/godot.x11.tools.64

# Detect when Godot terminates the project manager
wait
# Check if Godot has terminated, or if the editor has been started
if pgrep -x "godot.x11.tools" > /dev/null
then
    echo "Godot Editor Running"
    Check_Godot
else
    exit
fi






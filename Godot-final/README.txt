Steps to build image:

1. navigate to directory of the Godot dockerfile

2. chmod +x godot.x11.tools.64 && chmod +x Run_Godot.sh

3. sudo docker build -t godot .
	- Docker daemon creates the image and stores it in its repo.

4. sudo docker save -o godot.tar godot
	- Docker daemon exports the godot image as a file called godot.tar

Steps to run the Godot image:

1. chmod +x Setup_Script.sh && chmod +x tx2-docker

2. Change preference for executable text files to "Ask each time"

1. double click Setup_Script.sh
	- Paths in this script may need to be modified.

Notes:
 
- Once the image is built, you no longer need the godot executable.

- A volume from the host machine is mounted to the container. You can save your projects here if you want them stored in the host machine.
	- it is currently called My-Game-Projects in home directory.
- You can build your own Godot executable to use in this Docker image.

Requirements: 

- Docker ce. 

- To build: godot executable built on tx2.




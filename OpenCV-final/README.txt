Steps to build image:

1. sudo docker build -t opencv .

2. Do whatever one needs with the image.
	- For ex, run it, do stuff in container, commit it, export it, etc.

Steps to run the image:

1. Make sure the opencv image is loaded.

2. xhost +

3. sudo ./tx2-docker-opencv run opencv
	- make sure the file is executable.

Note:
- Assumption: user is experienced in terminal commands and familiar in software development environments.
- (IMPORTANT) A lot of the directories where the libraries and dependencies are stored in the host machine are mounted to the container.
	- The container directly uses the libraries available in the host machine. This is how OpenCV gets called in the container.
	- Installing software packages in the container may install them directly to the host machine. This is not recommended.
	- You can use other tools available in the host machine, like cmake or python.
- Use docker cp to move directories or files between host and container.
